<?php

namespace Drupal\media_riddle_marketplace;

/**
 * Interface RiddleMediaServiceInterface.
 *
 * @package Drupal\media_riddle_marketplace
 */
interface RiddleMediaServiceInterface {

  /**
   * Creates or updates all new riddles, for every riddle bundle.
   */
  public function createOrUpdateMediaEntities(): void;

  /**
   * Checks all riddle bundles and returns a list of new/updated items.
   *
   * @return array
   *   List of bundle => riddles.
   */
  public function getNewRiddles();

}
