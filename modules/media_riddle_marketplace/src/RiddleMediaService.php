<?php

namespace Drupal\media_riddle_marketplace;

use Drupal\Component\Utility\DiffArray;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\media\MediaInterface;
use Drupal\riddle_marketplace\RiddleClientInterface;

/**
 * Class RiddleFeedService.
 *
 * @package Drupal\riddle_marketplace
 */
class RiddleMediaService implements RiddleMediaServiceInterface {

  /**
   * The riddle feed service.
   *
   * @var \Drupal\riddle_marketplace\RiddleClientInterface
   */
  protected $riddleClient;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Riddle Media Service.
   *
   * Constructor.
   *
   * @param \Drupal\riddle_marketplace\RiddleClientInterface $riddleClient
   *   Riddle Feed service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Database\Connection $database
   *   The database service.
   */
  public function __construct(RiddleClientInterface $riddleClient, EntityTypeManagerInterface $entityTypeManager, Connection $database) {
    $this->riddleClient = $riddleClient;
    $this->entityTypeManager = $entityTypeManager;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public function createOrUpdateMediaEntities(): void {
    foreach ($this->getNewRiddles() as $type => $riddles) {
      /** @var \Drupal\media\MediaTypeInterface $type */
      $type = $this->entityTypeManager->getStorage('media_type')
        ->load($type);
      $sourceField = $type->get('source_configuration')['source_field'];
      foreach ($riddles as $riddle) {
        $data = [
          'type' => $type->id(),
          'source_field' => $sourceField,
          'riddle_id' => $riddle['id'],
          'riddle_title' => $riddle['title'],
        ];
        static::import($data);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getNewRiddles() {

    $feed = $this->riddleClient->getFeed();
    $riddle_feed_ids = array_keys($feed);
    $riddle_feed_items = [];
    foreach ($feed as $item) {
      $riddle_feed_items[$item['id']] = [
        'id' => $item['id'],
        'title' => $item['title'],
      ];
    }

    if (empty($riddle_feed_items)) {
      return [];
    }

    /** @var \Drupal\media\MediaTypeInterface[] $riddleBundles */
    $riddleBundles = $this->entityTypeManager->getStorage('media_type')
      ->loadByProperties([
        'source' => 'riddle_marketplace',
      ]);

    $newRiddles = [];
    foreach ($riddleBundles as $type) {

      $sourceField = $type->get('source_configuration')['source_field'];
      $query = $this->database->select("media__$sourceField", 'n');
      $query->join('media_field_data', 'd', 'd.mid = n.entity_id');
      $query->addField('n', "${sourceField}_value", 'id');
      $query->addField('d', 'name', 'title');
      $query->condition("n.${sourceField}_value", $riddle_feed_ids, 'IN');
      $existing_riddle_items = $query->execute()->fetchAllAssoc('id', \PDO::FETCH_ASSOC);

      $new_riddles = $this->arrayDiffMultidimensional($riddle_feed_items, $existing_riddle_items);
      // Sort oldest riddles to the top, so they will be created first.
      ksort($new_riddles);

      $newRiddles[$type->id()] = $new_riddles;
    }

    return $newRiddles;
  }

  /**
   * Computes the multidimensional difference of arrays.
   *
   * @param array $array1
   *   The array with new entries.
   * @param array $array2
   *   The old array.
   *
   * @return array
   *  All the entries from array1 that are not present in array2.
   */
  protected function arrayDiffMultidimensional(array $array1, array $array2): array {
    $diffItems = [];
    foreach ($array1 as $keyNew => $valueNew) {
      if (!isset($array2[$keyNew])) {
        $diffItems[$keyNew] = $valueNew;
        continue;
      }

      if (implode('', $valueNew) !== implode('', $array2[$keyNew])) {
        $diffItems[$keyNew] = $valueNew;
      }
    }

    return $diffItems;
  }

  /**
   * The import function, used by batch.
   *
   * @param array $data
   *   Containing keys bundle, source_field, riddle_id and riddle_title.
   */
  public static function import(array $data): void {
    /** @var \Drupal\media\MediaStorage $storage */
    $storage = \Drupal::entityTypeManager()->getStorage('media');
    $media = $storage->loadByProperties([$data['source_field'] => $data['riddle_id']]);
    $media = is_array($media) ? reset($media) : $media;
    if (
      $media instanceof MediaInterface
      && $media->label() !== $data['riddle_title']
    ) {
      $media->setName($data['riddle_title']);
      $media->save();
    }
    else {
      $storage->create([
        'bundle' => $data['type'],
        $data['source_field'] => $data['riddle_id'],
      ])->save();
    }
  }

}
