<?php

namespace Drupal\media_riddle_marketplace\Plugin\media\Source;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\media\MediaSourceBase;
use Drupal\media\MediaInterface;
use Drupal\riddle_marketplace\RiddleClientInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides media type plugin for Riddle.
 *
 * @MediaSource(
 *   id = "riddle_marketplace",
 *   label = @Translation("Riddle Marketplace"),
 *   description = @Translation("Provides business logic and metadata for riddle marketplace."),
 *   allowed_field_types = {"string", "string_long"},
 *   default_thumbnail_filename = "riddle.png"
 * )
 */
class Riddle extends MediaSourceBase {

  const ALLOWED_EXTENSIONS = ['jpg', 'jpeg', 'png', 'gif', 'webp'];

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Riddle feed service.
   *
   * @var \Drupal\riddle_marketplace\RiddleClientInterface
   */
  protected RiddleClientInterface $riddleClient;

  /**
   * Constructs a new class instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity field manager service.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type plugin manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \GuzzleHttp\Client $http_client
   *   The http client service.
   * @param \Drupal\riddle_marketplace\RiddleClientInterface $riddleClient
   *   Riddle feed service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, FieldTypePluginManagerInterface $field_type_manager, ConfigFactoryInterface $config_factory, FileSystemInterface $file_system, Client $http_client, RiddleClientInterface $riddleClient) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $entity_field_manager, $field_type_manager, $config_factory);
    $this->fileSystem = $file_system;
    $this->httpClient = $http_client;
    $this->riddleClient = $riddleClient;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('config.factory'),
      $container->get('file_system'),
      $container->get('http_client'),
      $container->get('riddle_marketplace.client'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    return [
      'id' => $this->t('Riddle id'),
      'status' => $this->t('Publishing status'),
      'thumbnail' => $this->t('Link to the thumbnail'),
      'thumbnail_local' => $this->t("Copies thumbnail locally and return it's URI"),
      'thumbnail_local_uri' => $this->t('Returns local URI of the thumbnail'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $name) {

    $code = NULL;
    if (isset($this->configuration['source_field'])) {
      $source_field = $this->configuration['source_field'];
      if ($media->hasField($source_field) && $media->{$source_field}->first()) {
        $property_name = $media->{$source_field}->first()->mainPropertyName();
        $code = $media->{$source_field}->{$property_name};
      }
    }

    if (!$code) {
      return FALSE;
    }

    if ($name == 'id') {
      return $code;
    }

    $riddle = $this->riddleClient->getRiddle($code);

    // If we have auth settings return the other fields.
    if ($riddle) {
      switch ($name) {
        case 'title':
        case 'default_name':
          if (isset($riddle['title'])) {
            return $riddle['title'];
          }
          return NULL;

        case 'status':
          if (isset($riddle['status'])) {
            return $riddle['status'];
          }
          return NULL;

        case 'thumbnail':
          if (isset($riddle['image'])) {
            return $riddle['image'];
          }
          return NULL;

        case 'thumbnail_local':
        case 'thumbnail_local_uri':
        case 'thumbnail_uri':
          if (isset($riddle['image'])) {
            $directory = $this->configFactory->get('media_riddle_marketplace.settings')->get('local_images');
            // Try to load from local filesystem.
            $absolute_uri = '';
            foreach (self::ALLOWED_EXTENSIONS as $extension) {
              if ($realpath = $this->fileSystem->realpath("{$directory}/{$code}.{$extension}")) {
                $absolute_uri = $realpath;
                break;
              }
            }
            if ($absolute_uri) {
              return $directory . '/' . $this->fileSystem->basename($absolute_uri);
            }
            $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
            // Get image from remote and save locally.
            try {
              $response = $this->httpClient->get($riddle['image']);
              $format = pathinfo($riddle['image'], PATHINFO_EXTENSION);
              if (
                "image/{$format}" === $response->getHeaderLine('Content-Type')
                && in_array($format, self::ALLOWED_EXTENSIONS, TRUE)
              ) {
                return $this->fileSystem->saveData($response->getBody(), $directory . '/' . $code . "." . $format, FileSystemInterface::EXISTS_REPLACE);
              }
            }
            catch (ClientException $e) {
              // Do nothing.
            }
          }
          return NULL;
      }
    }

    return NULL;
  }

}
