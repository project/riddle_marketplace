<?php

namespace Drupal\riddle_marketplace;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Utility\Error;
use Drupal\riddle_marketplace\Exception\NoApiKeyException;
use Riddle\Api\Client;

/**
 * Class RiddleFeedService.
 *
 * @package Drupal\riddle_marketplace
 */
class RiddleClient implements RiddleClientInterface {

  use LoggerChannelTrait;

  /**
   * Cache Service to store Riddle Feed.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private $cacheService;

  /**
   * Riddle Marketplace Module configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private $moduleSettings;

  /**
   * Cache validity period.
   *
   * Mainly used to reduce number of requests to Riddle
   * and to keep fast response for user.
   *
   * - period has to be valid for DrupalDateTime::modify method
   * - period should be less then time required to add new Riddle entry,
   *   so that client after adding entry in Riddle can find it in search here.
   *
   * @var string
   */
  private static $cachePeriod = '30 seconds';

  /**
   * Generic title used for Riddles without defined title.
   *
   * -> Riddle UID will be appended at end of it.
   *
   * @var string
   */
  private $emptyTitlePrefix;

  /**
   * The Riddle API client.
   *
   * @var \Riddle\Api\Client|null
   */
  protected ?Client $client = NULL;

  /**
   * Riddle Feed Service.
   *
   * Constructor.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheService
   *   Cache service created for caching of Riddle Feed.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configService
   *   Configuration Factory.
   */
  public function __construct(CacheBackendInterface $cacheService, ConfigFactoryInterface $configService) {
    $this->cacheService = $cacheService;

    // Fetch and store settings for this module.
    $this->moduleSettings = $configService->get('riddle_marketplace.settings');

    // Set Empty Title Prefix.
    $this->emptyTitlePrefix = $this->moduleSettings->get('riddle_marketplace.empty_title_prefix');
  }

  /**
   * {inheritdoc}
   *
   * @throws \Drupal\riddle_marketplace\Exception\NoApiKeyException
   */
  public function getClient(): Client {
    if (empty($this->client)) {
      $this->client = new Client($this->getToken());
    }
    return $this->client;
  }

  /**
   * {@inheritdoc}
   */
  public function getFeed(): array {
    $token = $this->getToken();
    $cacheId = 'riddle_marketplace.feed:' . $token;

    if ($cache = $this->cacheService->get($cacheId)) {
      $feed = $cache->data;
    }
    else {
      $feed = [];
      $projects = $this->moduleSettings->get('riddle_marketplace.projects') ?? [NULL];
      foreach ($projects as $project) {
        $page = 1;
        $riddleResponse = $this->fetchRiddles($page, $project);
        $feed += $this->processRiddlesResponse($riddleResponse);
        do {
          $page++;
          $riddleResponse = $this->fetchRiddles($page, $project);
          $feed += $this->processRiddlesResponse($riddleResponse);
        } while (!empty($riddleResponse[0]));
      }

      $cacheExpire = $this->getCacheExpireTimestamp();
      $this->cacheService->set($cacheId, $feed, $cacheExpire);
    }
    return $feed;
  }

  /**
   * Get Riddle Token from riddle_marketplace settings.
   *
   * @return string
   *   Return defined Riddle Token.
   */
  private function getToken(): string {
    $token = $this->moduleSettings->get('riddle_marketplace.token');
    if (!$token) {
      throw new NoApiKeyException();
    }
    return $token;
  }

  /**
   * Fetches feed from Riddle API.
   *
   * @param int|null $page
   *   (optional) Page number, default 1.
   * @param int|null $project
   *   (optional) The project id.
   *
   * @return array
   *   JSON decoded Riddle API result.
   */
  private function fetchRiddles(?int $page = 1, ?int $project = NULL): array {
    $response = [];

    try {
      $client = $this->getClient();
      $response = $client->riddle()->list($project, NULL, NULL, NULL, NULL, 'created', 'DESC', $page);
    }
    catch (\Exception $e) {
      $logger = $this->getLogger('riddle_marketplace');
      Error::logException($logger, $e);
    }

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function getRiddle(string $id): ?array {
    $response = [];

    try {
      $client = $this->getClient();
      $response = $client->riddle()->getRiddle($id);
    }
    catch (\Exception $e) {
      $logger = $this->getLogger('riddle_marketplace');
      Error::logException($logger, $e);
    }

    return $this->processRiddleResponse($response);
  }

  /**
   * {@inheritdoc}
   */
  public function getProjects(): array {
    $response = [];

    try {
      $client = $this->getClient();
      $response = $client->project()->list();
    }
    catch (\Exception $e) {
      $logger = $this->getLogger('riddle_marketplace');
      Error::logException($logger, $e);
    }

    return $response;
  }

  /**
   * Process riddles response from Riddle API.
   *
   * Response is in JSON format. Method will return only relevant data
   * for internal feed cached storage.
   * - currently: uid, title.
   *
   * @param array $riddleResponse
   *   Riddle API Result.
   *
   * @return array
   *   Filtered Riddle Feed with params relevant for the module.
   */
  private function processRiddlesResponse(array $riddleResponse): array {
    $feed = [];
    if (!empty($riddleResponse)) {
      foreach ($riddleResponse as $riddleEntry) {
        if ($processed = $this->processRiddleResponse($riddleEntry)) {
          $feed[$riddleEntry['UUID']] = $processed;
        }
      }
    }

    return $feed;
  }

  /**
   * Process riddle response from Riddle API.
   *
   * @param array $riddleEntry
   *   Riddle API Result.
   *
   * @return array|null
   *   Filtered Riddle Feed entry with params relevant for the module.
   */
  protected function processRiddleResponse(array $riddleEntry): ?array {
    if (!$this->isValidRiddleFeedEntry($riddleEntry)) {
      // Skip invalid riddle feed entries.
      return NULL;
    }

    return [
      'title' => $this->getRiddleTitle($riddleEntry),
      'id' => $riddleEntry['UUID'],
      // All riddles in response are published atm.
      'status' => 1,
      'image' => $riddleEntry['image'] ?? NULL,
      'data' => $riddleEntry['data'] ?? [],
    ];
  }

  /**
   * Validation Riddle Feed Entry.
   *
   * @param array|null $riddleEntry
   *   Single Riddle Feed Entry.
   *
   * @return bool
   *   Result of validation.
   */
  private function isValidRiddleFeedEntry(?array $riddleEntry): bool {
    if (
      empty($riddleEntry) || !is_array($riddleEntry) || empty($riddleEntry['UUID'])
    ) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Returns Riddle Title.
   *
   * From feed entry return Title
   * - in case title is not defined use generic name.
   *
   * @param array $riddleEntry
   *   Single Riddle Feed Entry.
   *
   * @return string
   *   Riddle element title.
   */
  private function getRiddleTitle(array $riddleEntry): string {
    if (!empty($riddleEntry['title'])) {
      return html_entity_decode($riddleEntry['title']);
    }
    return $this->emptyTitlePrefix . $riddleEntry['id'];
  }

  /**
   * Get cache validity end timestamp.
   *
   * @return int
   *   Timestamp of expire time for Cache.
   */
  private function getCacheExpireTimestamp(): int {
    /* @var \DateTime $date */
    $date = new DrupalDateTime();
    $date->modify('+' . static::$cachePeriod);

    return $date->getTimestamp();
  }

}
