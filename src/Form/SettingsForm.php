<?php

namespace Drupal\riddle_marketplace\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\riddle_marketplace\RiddleClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form that configures riddle_marketplace settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The Riddle feed service.
   *
   * @var \Drupal\riddle_marketplace\RiddleClientInterface
   */
  protected RiddleClientInterface $riddleClient;

  /**
   * Constructs a Riddle settings form object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\riddle_marketplace\RiddleClientInterface $riddleClient
   *   The Riddle feed service
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    RiddleClientInterface $riddleClient
  ) {
    parent::__construct($config_factory);
    $this->riddleClient = $riddleClient;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('riddle_marketplace.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'riddle_marketplace_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->configFactory()->get('riddle_marketplace.settings');

    $form['token'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Riddle token'),
      '#description' => $this->t('Register a new account at <a href=":riddle" target="_blank">riddle.com</a> and get a <em>token</em> by enabling the API at <a href=":api" target="_blank">API keys </a>.', [
        ':riddle' => 'http://www.riddle.com',
        ':api' => 'https://www.riddle.com/creator/account/access-token',
      ]),
      '#default_value' => $settings->get('riddle_marketplace.token'),
    ];

    if ($options = $this->getProjectOptions()) {
      $form['projects'] = [
        '#type' => 'checkboxes',
        '#multiple' => TRUE,
        '#options' => $options,
        '#title' => $this->t('Riddle projects'),
        '#description' => $this->t('If you want to get the riddles from specific projects, select them here. If not set, only the riddles from the current user/project will be fetched (depending on the <a href=":api" target="_blank">API key permissions</a>)', [
          ':api' => 'https://www.riddle.com/creator/account/access-token',
        ]),
        '#default_value' => $settings->get('riddle_marketplace.projects') ?? [],
      ];
    }
    else {
      $form['projects'] = [
        '#type' => 'item',
        '#title' => $this->t('Riddle projects'),
      ];
      $message = $this->t('Please set a proper API token to see available projects.');
      $form['warning'] = [
        '#markup' => '<div class="messages messages--warning">' . $message . '</div>',
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * Gets available project options from Riddle.
   *
   * @return array
   *   The fetched project options.
   */
  protected function getProjectOptions(): array {
    $projects = $this->riddleClient->getProjects();
    $options = [];
    foreach ($projects as $project) {
      $options[$project['id']] = $project['name'];
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $values = $form_state->getValues();
    $config = $this->configFactory()->getEditable('riddle_marketplace.settings');
    $config->set('riddle_marketplace.token', $values['token']);
    $config->set('riddle_marketplace.projects', $values['projects']);
    $config->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'riddle_marketplace.settings',
    ];
  }

}
