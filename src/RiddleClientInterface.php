<?php

namespace Drupal\riddle_marketplace;

use Riddle\Api\Client;

/**
 * Interface RiddleFeedServiceInterface.
 *
 * @package Drupal\riddle_marketplace
 */
interface RiddleClientInterface {

  /**
   * Gets Riddle API client.
   *
   * @return \Riddle\Api\Client|null
   *   The Riddle API client.
   */
  public function getClient(): ?Client;

  /**
   * Return feed for configured Riddle Account (Token).
   *
   * @return array
   *   Return feed from Riddle API Service or NULL if feed is not available.
   */
  public function getFeed(): array;

  /**
   * Gets Riddle from Riddle API.
   *
   * @param string $id
   *   Riddle id.
   *
   * @return array|null
   *   JSON decoded Riddle API result.
   */
  public function getRiddle(string $id): ?array;

  /**
   * Gets projects from Riddle API.
   *
   * @return array
   *   Projects ID-name array.
   */
  public function getProjects(): array;

}
